//! Bamboo region storing in memory, reading, and writing to disk.

mod fs;

use super::{ChunkExistsGuard, CountedChunk, MultiChunk};
use bb_common::math::ChunkPos;
use parking_lot::{Mutex, RwLock, RwLockReadGuard, RwLockWriteGuard};
use std::collections::HashMap;

/// The same structure as a chunk position, but used to index into a region. Can
/// be converted to/from a `ChunkPos` by multiplying/dividing its coordinates by
/// 32.
#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
pub struct RegionPos {
  pub x: i32,
  pub z: i32,
}

/// A chunk position within a region. The X and Z cannot be outside `0..32`.
#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
pub struct RegionRelPos {
  x: u8,
  z: u8,
}

pub struct RegionMap {
  /// The actual chunks in the world.
  pub chunks: RwLock<HashMap<ChunkPos, CountedChunk>>,

  /// A map of each region file.
  pub regions: Mutex<HashMap<RegionPos, Region>>,

  /// Controls if the world is saved or not.
  pub save: bool,
}

pub struct Region {
  pos:  RegionPos,
  save: bool,
}

/// Temporary struct to hold the data that needs to be saved.
struct RegionToSave<'a> {
  pos: RegionPos,

  /// An array of `32*32 = 1024` chunks. The index is `x + z * 32`.
  chunks: Box<[Option<&'a CountedChunk>; 1024]>,
}

impl RegionMap {
  pub fn new(save: bool) -> Self {
    RegionMap { chunks: RwLock::new(HashMap::new()), regions: Mutex::new(HashMap::new()), save }
  }

  pub fn chunk(&self, new_chunk: impl FnOnce() -> MultiChunk, pos: ChunkPos) -> ChunkExistsGuard {
    // Read the chunk, and call `f` if the chunk exists.
    {
      let chunks = self.chunks.read();
      if chunks.contains_key(&pos) {
        let guard = RwLockReadGuard::map(chunks, |c| c.get(&pos).unwrap());
        return ChunkExistsGuard { guard };
      }
    }

    // Otherwise, acquire a write lock, and insert a new chunk.
    {
      let mut chunks = self.chunks.write();
      if chunks.get(&pos).is_none() {
        chunks.insert(pos, CountedChunk::new(new_chunk()));
      }

      let chunks = RwLockWriteGuard::downgrade(chunks);
      let guard = RwLockReadGuard::map(chunks, |c| c.get(&pos).unwrap());
      ChunkExistsGuard { guard }
    }
  }

  pub fn chunk_opt(&self, pos: ChunkPos) -> Option<ChunkExistsGuard> {
    let chunks = self.chunks.read();
    if chunks.contains_key(&pos) {
      let guard = RwLockReadGuard::map(chunks, |c| c.get(&pos).unwrap());
      Some(ChunkExistsGuard { guard })
    } else {
      None
    }
  }

  pub fn has_chunk(&self, pos: ChunkPos) -> bool { self.chunks.read().contains_key(&pos) }

  pub fn unload_chunks(&self) {
    let mut unloadable = vec![];
    {
      for (&pos, chunk) in self.chunks.read().iter() {
        // Optimistically count all the chunks that can be unloaded.
        if chunk.count.load(std::sync::atomic::Ordering::Acquire) == 0 {
          unloadable.push(pos);
        }
      }
    }
    if !unloadable.is_empty() {
      let mut wl = self.chunks.write();
      for pos in unloadable {
        // Now that we have a write lock, we re-validate to make sure we don't unload
        // anything we weren't supposed to.
        if wl.get(&pos).is_some_and(|c| c.count.load(std::sync::atomic::Ordering::Acquire) == 0) {
          wl.remove(&pos);
        }
      }
    }
  }

  pub fn save(&self) {
    if !self.save {
      info!("saving disabled, skipping");
      return;
    }
    info!("saving world...");
    let lock = self.regions.lock();
    for region in lock.values() {
      self.save_region(region);
    }
    info!("saved");
  }

  fn save_region(&self, region: &Region) {
    if !region.save {
      return;
    }

    let chunks = self.chunks.read();

    const NONE: Option<&CountedChunk> = None;
    let mut to_save = RegionToSave { pos: region.pos, chunks: Box::new([NONE; 1024]) };

    for (&pos, chunk) in chunks.iter() {
      if RegionPos::from(pos) != region.pos {
        continue;
      }

      let rel = RegionRelPos::from(pos);
      to_save.chunks[rel.x as usize + rel.z as usize * 32] = Some(chunk);
    }
    to_save.save();
  }
}

impl Region {
  pub fn new_no_load(pos: RegionPos, save: bool) -> Self { Region { pos, save } }
}

impl RegionPos {
  pub fn new(chunk: ChunkPos) -> Self {
    RegionPos {
      x: if chunk.x() < 0 { (chunk.x() + 1) / 32 - 1 } else { chunk.x() / 32 },
      z: if chunk.z() < 0 { (chunk.z() + 1) / 32 - 1 } else { chunk.z() / 32 },
    }
  }
}

impl RegionRelPos {
  pub fn new(chunk: ChunkPos) -> Self {
    RegionRelPos { x: ((chunk.x() % 32 + 32) % 32) as u8, z: ((chunk.z() % 32 + 32) % 32) as u8 }
  }
}

impl From<ChunkPos> for RegionPos {
  fn from(chunk: ChunkPos) -> Self { RegionPos::new(chunk) }
}
impl From<ChunkPos> for RegionRelPos {
  fn from(chunk: ChunkPos) -> Self { RegionRelPos::new(chunk) }
}
